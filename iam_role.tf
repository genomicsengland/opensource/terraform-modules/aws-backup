resource "aws_iam_role" "backup_iam_role" {
  name                 = "${local.resource_prefix}iam_role"
  permissions_boundary = var.iam_boundary != "" ? var.iam_boundary : null
  assume_role_policy   = data.aws_iam_policy_document.backup_iam_assume.json

  inline_policy {
    name   = "tag_get_resources"
    policy = data.aws_iam_policy_document.tag_get_resources.json
  }
}

data "aws_iam_policy_document" "backup_iam_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}
resource "aws_iam_role_policy_attachment" "backup_iam_role_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_iam_role.name
}

resource "aws_iam_role_policy_attachment" "restore_iam_role_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"
  role       = aws_iam_role.backup_iam_role.name
}

resource "aws_kms_grant" "grant_encryption_key_for_backup" {
  count             = var.iam_encryption_kms_key == "" ? 0 : 1
  name              = "${local.resource_prefix}key-grant"
  key_id            = var.iam_encryption_kms_key
  grantee_principal = aws_iam_role.backup_iam_role.arn
  operations        = var.kms_grant_operations
}

resource "aws_iam_role_policy_attachment" "s3_backup_iam_role_attachment" {
  count      = var.enable_s3_backups ? 1 : 0
  policy_arn = aws_iam_policy.s3_backup_policy[0].arn
  role       = aws_iam_role.backup_iam_role.name
}

resource "aws_iam_role_policy_attachment" "s3_restore_iam_role_attachment" {
  count      = var.enable_s3_backups ? 1 : 0
  policy_arn = aws_iam_policy.s3_restore_policy[0].arn
  role       = aws_iam_role.backup_iam_role.name
}

data "aws_iam_policy_document" "tag_get_resources" {
  statement {
    actions   = ["tag:GetResources"]
    resources = ["*"]
  }
}
