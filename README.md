# AWS Backup Module

Creates a set of resources for creating and managing backup via AWS Backup service.

Example of use can be found in [example](../example) directory

## Required parameters

* `default_tags` - Map of tags inherited from top - level project. If there aren't any, just provide and empty map.
* `resource_prefix` - String that should be put in front of names of resources created by this module.
* `vault_encryption_kms_key` - ARN of KMS Key that will be used to encrypt the backups in vault.
* `region` - AWS region where the module will be applied.
* `backup_plans` - list of object that describe the backup plans, for example: ```{ name = "ec2_daily", schedule = "cron(0 12 * * ? *)", cold_storage = 60, delete = 150, start_window = 60, completion_window = 180 },``` name will later be used as a value for tag that sets the resource to be backed up by that particular plan. Name and schedule are required, start_window, completion_window, cold_storage and delete are optional.

## Optional parameters

* `backup_plan_tag_name` - List of names for Tags that will be used to associate resources with backup plans. The default value is: `["aws_backup_plan"]`
* `use_sns_messaging`    - Should a resources for monitoring the backup process be created. Defaults to `true`
* `backup_notify_events` - List of events for which a message in SNS will be send. Default is: `["BACKUP_JOB_STARTED", "BACKUP_JOB_COMPLETED", "RESTORE_JOB_STARTED", "RESTORE_JOB_COMPLETED"]`
* `vault_lock_enabled`   - Should the snapshots be additinally protected by vault_lock. Default is `false`
* `vault_lock_settings`  - Settings for vault lock. Defaults is: `{ changeable_for_days = 3, max_retention_days  = 365, min_retentions_days = 14 }`
* `vault_policy_allow`   - Set of allow permissions on backup vault, who and what can do with it, default is: `[ { arn = "*", allow = [ "backup:DescribeBackupVault", "backup:GetBackupVaultNotifications" ] }]`
* `vault_policy_deny`    - Set of deny permissions on backup, who and what cannot be done on vault.
* `snapshot_policy_allow` - Set of allow permission for snapshots, who and what can do with created snapshots, i. e. restore or delete them.
* `snapshot_policy_deny`  - Set of deny permissions for snapshots, who and what cannot be done with snapshots, i. e. enforce no one can remove snapshots.
* `iam_boundary`          - Set the IAM Boundary for backup vault
* `iam_encryption_kms_key` - KMS Key needed to perform Backup Restore jobs with the IAM Role, Creates a KMS Grant for IAM role on KMS Key
* `kms_grant_operations`   - Set of operations allowed by KMS Grant created by `iam_encryption_kms_key`. Default is `["Encrypt", "GenerateDataKeyWithoutPlaintext"]`
* `enable_s3_backups`     - Creates additional set of IAM Policies that enables backing up S# Buckets, The Bucket needs to have versioning enabled in order to be backed up. Defaults to false.
* `opt_in_preference`     - Controls which services are subject to Backup plans. You can globally enable or disable backups for certain services.
* `sns_encryption_kms_key` - Encrypts the SNS topic.

## Outputs

* `backup_vault_name` - Name of the backup vault, created with resource prefix and internal name within module
* `backup_vault_arn`  - ARN of the backup vault
* `backup_sns_topic_name` - Name of the SNS topic created for the backup vault. Only gets created when `use_sns_messaging` is set to `true`. Used when setting up messaging integration to OpsGenie, Slack, etc.
* `backup_sns_topic_arn`  - ARN of the SNS topic.

## Terraform tests

__Note:__ This testing framework is available in Terraform v1.6.0 and later - [Guide](https://developer.hashicorp.com/terraform/language/tests).

### How-to

__Note:__ You need to configure AWS CLI credentials and point it to a **NON_PROD** account as these tests can lead to deploying infrastructure if the command is *apply*.

* From the root of the module.
    * Run: **terraform init**
    * Run: **Terraform test**