resource "aws_backup_vault" "backup_vault" {
  name = "${local.resource_prefix}vault"
  tags = merge(
    local.default_tags,
    tomap({ "Name" = "${local.resource_prefix}vault" })
  )
  kms_key_arn = var.vault_encryption_kms_key
}
