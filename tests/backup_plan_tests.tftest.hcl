variables {
  default_tags = {
    "terraform_test" = true
  }

  resource_prefix = "terraform_test"

  vault_encryption_kms_key = "arn:aws:kms:eu-west-2:111122223333:key/"

  region = "eu-west-2"

  backup_plans = [{ name = "ec2_daily", schedule = "cron(0 12 * * ? *)", cold_storage = 60, delete = 150, start_window = 60, completion_window = 180 }]
}

provider "aws" {
  region = "eu-west-2"
}

run "backup_continuous_tests" {
  command = plan

  assert {
    condition = alltrue([
      for rule in aws_backup_plan.backup_plan[0].rule : rule.enable_continuous_backup == false
    ])
    error_message = "Continuous backups should be false by default"
  }
}