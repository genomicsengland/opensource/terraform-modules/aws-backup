resource "aws_backup_vault_lock_configuration" "vault_lock" {
  count               = var.vault_lock_enabled ? 1 : 0
  backup_vault_name   = aws_backup_vault.backup_vault.name
  changeable_for_days = var.vault_lock_settings["changeable_for_days"]
  max_retention_days  = var.vault_lock_settings["max_retention_days"]
  min_retention_days  = var.vault_lock_settings["min_retention_days"]
}
