resource "aws_backup_plan" "backup_plan" {
  count = length(var.backup_plans)
  name  = "${local.resource_prefix}plan_${var.backup_plans[count.index].name}"

  tags = merge(
    local.default_tags,
    tomap({ "Name" = "${local.resource_prefix}plan_${var.backup_plans[count.index].name}" })
  )

  rule {
    target_vault_name        = aws_backup_vault.backup_vault.name
    rule_name                = "${local.resource_prefix}plan_${var.backup_plans[count.index].name}"
    schedule                 = var.backup_plans[count.index].schedule
    enable_continuous_backup = var.enable_continuous_backup

    start_window      = lookup(var.backup_plans[count.index], "start_window", null)
    completion_window = lookup(var.backup_plans[count.index], "completion_window", null)

    dynamic "lifecycle" {
      for_each = lookup(var.backup_plans[count.index], "cold_storage", null) == null && lookup(var.backup_plans[count.index], "delete", null) == null ? [] : ["*"]
      content {
        cold_storage_after = lookup(var.backup_plans[count.index], "cold_storage", null)
        delete_after       = lookup(var.backup_plans[count.index], "delete", null)
      }
    }

    //To be added
    /*
    recovery_point_tags =
    copy_action {}
    */
  }
}
