variable "default_tags" {
  type        = map(any)
  description = "Map of tags to be added to resources in this module, usually inherited from component where the modules is used"
}

variable "resource_prefix" {
  type        = string
  description = "A Prefix to be added to each resource created by this module, for ease of identification where the resources belong"
}

variable "resource_suffix" {
  type        = string
  description = "A Suffix to be added to each resource created by this module, for ease of identification where the resources belong"
  default     = "backups"
}

variable "resource_separator" {
  type        = string
  description = "Separator to be used between prefix and suffix"
  default     = "-"
}
variable "vault_encryption_kms_key" {
  type        = string
  description = "ARN of KMS encryption key to be used to encrypt backups in vault"
}

variable "iam_encryption_kms_key" {
  type        = string
  default     = ""
  description = "KMS Key needed to perform Backup Restore jobs with the IAM Role"
}

variable "sns_encryption_kms_key" {
  type        = string
  default     = ""
  description = "ARN of KMS encryption key to be used to encrypt SNS topics"
}

variable "region" {
  type        = string
  description = "aws region"
}

variable "backup_plans" {
  type        = list(map(any))
  description = "List of maps with settings of backup plans"
}

variable "backup_plan_tag_name" {
  type        = list(string)
  description = "The name of the Tag to be used as for backup plan selection"
  default     = ["aws_backup_plan"]
}

variable "use_sns_messaging" {
  description = "Whether enable or disable vault notifications via SNS"
  default     = true
  type        = bool
}

variable "backup_notify_events" {
  type        = list(string)
  description = "List of events that a notification should be sent for"
  default     = ["BACKUP_JOB_STARTED", "BACKUP_JOB_COMPLETED", "RESTORE_JOB_STARTED", "RESTORE_JOB_COMPLETED"]
}

variable "vault_lock_enabled" {
  type        = bool
  default     = false
  description = "Should the backup vault lock be enabled"
}

variable "iam_boundary" {
  type        = string
  description = "Permissions boundary arn to add to the IAM role"
  default     = ""
}

variable "kms_grant_operations" {
  type        = list(string)
  description = "List of operations that KMS Grant will allow"
  default     = ["Encrypt", "GenerateDataKeyWithoutPlaintext"]
}

variable "enable_s3_backups" {
  type        = bool
  description = "Should the module also do S3 backups, creates a set of IAM Polices to enable that"
  default     = false
}

variable "enable_continuous_backup" {
  type        = bool
  description = "enables continuous backups which allows point in time recovery."
  default     = false
}

variable "opt_in_preference" {
  type        = map(string)
  description = "Control which services are covered under AWS Backups"
  default = {
    "aurora"          = true
    "cloudformation"  = false
    "documentdb"      = true
    "dynamodb"        = true
    "ebs"             = true
    "efs"             = true
    "ec2"             = true
    "efs"             = true
    "fsx"             = false
    "neptune"         = false
    "rds"             = true
    "redshift"        = false
    "s3"              = true
    "sap_hana_ec2"    = false
    "storage_gateway" = true
    "virtual_machine" = false
  }
}

variable "vault_lock_settings" {
  type = map(any)
  default = {
    changeable_for_days = 3,
    max_retention_days  = 365,
    min_retention_days  = 14,
  }
}

variable "vault_policy_allow" {
  description = "Default allow policies for Backup Vault"
  default     = []
  type        = set(object({ arn = string, allow = list(string) }))

  validation {
    condition     = anytrue([for obj in var.vault_policy_allow : obj.arn == "*" ? true : false]) == false
    error_message = "ARN in var.vault_policy_allow object must not be a wildcard."
  }
}

variable "vault_policy_deny" {
  type        = set(object({ arn = string, deny = list(string) }))
  default     = []
  description = "Default deny policies for Backup Vault"
}


variable "snapshot_policy_allow" {
  type        = set(object({ arn = string, allow = list(string) }))
  default     = []
  description = "Default allow policies for Snapshots created in Backup Vault"

  validation {
    condition     = anytrue([for obj in var.snapshot_policy_allow : obj.arn == "*" ? true : false]) == false
    error_message = "ARN in var.snapshot_policy_allow object must not be a wildcard."
  }
}


variable "snapshot_policy_deny" {
  type        = set(object({ arn = string, deny = list(string) }))
  default     = []
  description = "Default deny policies for Snapshots created in Backup Vault"
}
