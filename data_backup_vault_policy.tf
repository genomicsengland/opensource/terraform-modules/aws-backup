data "aws_iam_policy_document" "backup_policy" {
  dynamic "statement" {
    for_each = var.vault_policy_allow
    content {
      effect = "Allow"
      principals {
        type        = "AWS"
        identifiers = [statement.value.arn]
      }
      resources = [
        aws_backup_vault.backup_vault.arn
      ]
      actions = statement.value.allow
      sid     = "backup_vault_allow_${statement.value.arn}"
    }
  }

  dynamic "statement" {
    for_each = var.vault_policy_deny
    content {
      effect = "Deny"
      principals {
        type        = "AWS"
        identifiers = [statement.value.arn]
      }
      resources = [
        aws_backup_vault.backup_vault.arn
      ]
      actions = statement.value.deny
      sid     = "backup_vault_deny_${statement.value.arn}"
    }
  }

  dynamic "statement" {
    for_each = var.snapshot_policy_allow
    content {
      effect = "Allow"
      principals {
        type        = "AWS"
        identifiers = [statement.value.arn]
      }
      resources = [
        "arn:aws:ec2:${var.region}::snapshot/*",
        "arn:aws:ec2:${var.region}::image/*",
        "arn:aws:ebs:${var.region}::snapshot/*",
        "arn:aws:rds:${var.region}::snapshot/*",
        "arn:aws:efs:${var.region}::snapshot/*"
      ]
      actions = statement.value.allow
      sid     = "backup_snapshot_allow_${statement.value.arn}"
    }
  }

  dynamic "statement" {
    for_each = var.snapshot_policy_deny
    content {
      effect = "Deny"
      principals {
        type        = "AWS"
        identifiers = [statement.value.arn]
      }
      resources = [
        "arn:aws:ec2:${var.region}::snapshot/*",
        "arn:aws:ec2:${var.region}::image/*",
        "arn:aws:ebs:${var.region}::snapshot/*",
        "arn:aws:rds:${var.region}::snapshot/*",
        "arn:aws:efs:${var.region}::snapshot/*"
      ]
      actions = statement.value.deny
      sid     = "backup_snapshot_deny_${statement.value.arn}"
    }
  }
}
