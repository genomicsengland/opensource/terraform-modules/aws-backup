resource "aws_iam_policy" "s3_backup_policy" {
  count       = var.enable_s3_backups ? 1 : 0
  name        = "${local.resource_prefix}S3BucketBackupPolicy"
  description = "Policy for backing up S3 Buckets"
  policy      = data.aws_iam_policy_document.s3_backup_policy.json
  depends_on  = [aws_iam_role.backup_iam_role]
}

resource "aws_iam_policy" "s3_restore_policy" {
  count       = var.enable_s3_backups ? 1 : 0
  name        = "${local.resource_prefix}S3BucketRestorePolicy"
  description = "Policy for restoring S3 Buckets"
  policy      = data.aws_iam_policy_document.s3_restore_policy.json
  depends_on  = [aws_iam_role.backup_iam_role]
}
