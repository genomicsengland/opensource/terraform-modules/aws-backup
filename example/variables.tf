variable "region" {
  type        = string
  default     = "eu-west-2"
  description = "region"
}

variable "use_aws_backup" {
  type        = bool
  default     = true
  description = "Should the Backup module be used, you can use this switch to disable backup on certain envs"
}

variable "backup_plans" {
  type        = list(map(any))
  description = "List of maps with settings of backup plans"
  default = [
    { name = "ec2_daily", schedule = "cron(0 12 * * ? *)", cold_storage = 60, delete = 150, start_window = 60, completion_window = 180 },
    { name = "efs_daily", schedule = "cron(0 10 * * ? *)", cold_storage = 60, delete = 150, start_window = 60, completion_window = 180 },
    { name = "rds_daily", schedule = "cron(0 6 * * ? *)", cold_storage = 90, delete = 180, start_window = 60, completion_window = 180 },
    { name = "ec2_hourly", schedule = "cron(25 * * * ? *)", cold_storage = 60, delete = 150, start_window = 60, completion_window = 180 },
    { name = "default", schedule = "cron(0 12 * * ? *)" },
  ]
}
variable "vault_encryption_kms_key" {
  type        = string
  description = "KMS key used to encrypt backups in vault"
  default     = "arn:aws:kms:eu-west-2:366515835730:key/8e3c401b-1327-44b1-88dc-f020858e75b2"
}

variable "sns_encryption_kms_key" {
  type        = string
  default     = "arn:aws:kms:eu-west-2:366515835730:key/8e3c401b-1327-44b1-88dc-f020858e75b2"
  description = "ARN of KMS encryption key to be used to encrypt SNS topics"
}

variable "backup_plan_tag_name" {
  type    = string
  default = "aws_backup_plan"
}