module "aws_backup" {
  source = "../" # For use in actual code you can also reference using git link and branch or tag, example below
  # source = git::https://gitlab.com/genomicsengland/opensource/terraform-modules/aws-backup.git?ref=v1.0.0
  count                    = var.use_aws_backup ? 1 : 0 # In case you want to limit the use of module per environemnt
  default_tags             = local.default_tags
  resource_prefix          = local.resource_prefix
  vault_encryption_kms_key = var.vault_encryption_kms_key
  sns_encryption_kms_key   = var.sns_encryption_kms_key
  region                   = var.region

  backup_plans         = var.backup_plans
  backup_plan_tag_name = [var.backup_plan_tag_name]
  vault_lock_enabled   = false
  enable_s3_backups    = true

  vault_policy_allow = [
    {
      arn = "arn:aws:iam::366515835730:role/aws-reserved/sso.amazonaws.com/eu-west-2/AWSReservedSSO_AdministratorAccess_f8591283b466e34e",
      allow = [
        "backup:DescribeBackupVault",
        "backup:GetBackupVaultNotifications",
        "backup:ListRecoveryPointsByBackupVault",
        "backup:StartBackupJob",
      ],
    },

  ]
  vault_policy_deny = [
    {
      arn = "*",
      deny = [
        "backup:DeleteBackupVault",
      ]
    }
  ]
  snapshot_policy_allow = [
    {
      arn = "arn:aws:iam::366515835730:role/aws-reserved/sso.amazonaws.com/eu-west-2/AWSReservedSSO_AdministratorAccess_f8591283b466e34e",
      allow = [
        "backup:StartRestoreJob",
        "backup:DescribeRecoveryPoint"
      ],
    },
  ]
  snapshot_policy_deny = [
    {
      arn = "*",
      deny = [
        "backup:DeleteRecoveryPoint",
      ]
    }
  ]

}
