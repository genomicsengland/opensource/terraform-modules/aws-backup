resource "aws_instance" "backup_test_instance" {
  ami           = "ami-008485ca60c91a0f3" # official 19.04
  instance_type = "t2.micro"

  tags = merge(
    local.default_tags,
    map(
      "Name", "backup_test_instance",
    ),
  )

  root_block_device {
    volume_size           = "20"
    encrypted             = true
    delete_on_termination = true

    tags = merge(
      local.default_tags,
      map(
        "Name", "backup_test_instance",
        #var.backup_plan_tag_name, "ec2_daily"
      ),
    )
  }

  instance_initiated_shutdown_behavior = "stop"
}
