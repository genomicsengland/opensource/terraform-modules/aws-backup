locals {
  default_tags = map(
    "Project", "AWS_Backup",
    "Team", "CE",
    "Owner", "Lukasz Kwasniewski"
  )
  resource_prefix = "ce_test"
}
