resource "aws_s3_bucket" "test_backup_bucket" {
  bucket = "gel-backups-testing-s3-bucket"
  versioning {
    enabled = true
  }
  tags = merge(
    local.default_tags,
    map(
      "Name", "gel-backups-testing-s3-bucket",
      var.backup_plan_tag_name, "ec2_daily"
    ),
  )
}
