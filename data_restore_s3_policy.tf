data "aws_iam_policy_document" "s3_restore_policy" {
  statement {
    sid = "S3BucketRestorePermissions"
    actions = [
      "s3:CreateBucket",
      "s3:ListBucketVersions",
      "s3:ListBucket",
      "s3:GetBucketVersioning",
      "s3:GetBucketLocation",
      "s3:PutBucketVersioning",
      "s3:GetBucketOwnershipControls"
    ]
    resources = [
      "arn:aws:s3:::*"
    ]
  }

  statement {
    sid = "S3ObjectRestorePermissions"
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:DeleteObject",
      "s3:PutObjectVersionAcl",
      "s3:GetObjectVersionAcl",
      "s3:GetObjectTagging",
      "s3:PutObjectTagging",
      "s3:GetObjectAcl",
      "s3:PutObjectAcl",
      "s3:PutObject",
      "s3:ListMultipartUploadParts"
    ]
    resources = [
      "arn:aws:s3:::*/*"
    ]
  }

  statement {
    sid = "S3KMSPermissions"
    actions = [
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:GenerateDataKey"
    ]
    resources = ["*"]

    condition {
      test     = "StringLike"
      variable = "kms:ViaService"
      values = [
        "s3.*.amazonaws.com"
      ]
    }
  }
}
