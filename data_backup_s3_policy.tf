data "aws_iam_policy_document" "s3_backup_policy" {
  statement {
    sid = "S3BucketBackupPermissions"

    actions = [
      "s3:GetInventoryConfiguration",
      "s3:PutInventoryConfiguration",
      "s3:ListBucketVersions",
      "s3:ListBucket",
      "s3:GetBucketVersioning",
      "s3:GetBucketNotification",
      "s3:PutBucketNotification",
      "s3:GetBucketLocation",
      "s3:GetBucketTagging"
    ]

    resources = [
      "arn:aws:s3:::*"
    ]
  }

  statement {
    sid = "S3ObjectBackupPermissions"

    actions = [
      "s3:GetObjectAcl",
      "s3:GetObject",
      "s3:GetObjectVersionTagging",
      "s3:GetObjectVersionAcl",
      "s3:GetObjectTagging",
      "s3:GetObjectVersion"
    ]

    resources = [
      "arn:aws:s3:::*/*"
    ]
  }

  statement {
    sid = "KmsBackupPermissions"

    actions = [
      "kms:Decrypt",
      "kms:DescribeKey"
    ]

    condition {
      test     = "StringLike"
      variable = "kms:ViaService"
      values = [
        "s3.*.amazonaws.com"
      ]
    }

    resources = ["*"]
  }

  statement {
    sid = "EventsPermission"
    actions = [
      "events:DescribeRule",
      "events:EnableRule",
      "events:PutRule",
      "events:DeleteRule",
      "events:PutTargets",
      "events:RemoveTargets",
      "events:ListTargetsByRule",
      "events:DisableRule"
    ]

    resources = [
      "arn:aws:events:*:*:rule/AwsBackupManagedRule*"
    ]
  }

  statement {
    sid = "EventsMetricsGlobalPermissions"
    actions = [
      "cloudwatch:GetMetricData",
      "events:ListRules"
    ]
    resources = ["*"]
  }
}
