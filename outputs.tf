output "backup_vault_name" {
  value = aws_backup_vault.backup_vault.name
}

output "backup_vault_arn" {
  value = aws_backup_vault.backup_vault.arn
}

output "backup_sns_topic_name" {
  value = var.use_sns_messaging ? aws_sns_topic.backup_sns_topic[0].name : null
}

output "backup_sns_topic_arn" {
  value = var.use_sns_messaging ? aws_sns_topic.backup_sns_topic[0].arn : null
}
