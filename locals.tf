locals {
  default_tags = merge(
    var.default_tags,
    tomap({ "Module" = "aws_backup" })
  )
  resource_prefix = "${var.resource_prefix}${var.resource_separator}${var.resource_suffix}${var.resource_separator}"
}
