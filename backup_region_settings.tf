resource "aws_backup_region_settings" "backup_region_settings" {
  resource_type_opt_in_preference = {
    "Aurora"                 = var.opt_in_preference.aurora
    "CloudFormation"         = var.opt_in_preference.cloudformation
    "DocumentDB"             = var.opt_in_preference.documentdb
    "DynamoDB"               = var.opt_in_preference.dynamodb
    "EBS"                    = var.opt_in_preference.ebs
    "EC2"                    = var.opt_in_preference.ec2
    "EFS"                    = var.opt_in_preference.efs
    "FSx"                    = var.opt_in_preference.fsx
    "Neptune"                = var.opt_in_preference.neptune
    "RDS"                    = var.opt_in_preference.rds
    "Redshift"               = var.opt_in_preference.redshift
    "S3"                     = var.opt_in_preference.s3
    "SAP HANA on Amazon EC2" = var.opt_in_preference.sap_hana_ec2
    "Storage Gateway"        = var.opt_in_preference.storage_gateway
    "VirtualMachine"         = var.opt_in_preference.virtual_machine
  }
}
