resource "aws_backup_selection" "backup_selection" {
  count        = length(var.backup_plans)
  iam_role_arn = aws_iam_role.backup_iam_role.arn
  name         = "${local.resource_prefix}selection_${var.backup_plans[count.index].name}"
  plan_id      = aws_backup_plan.backup_plan[count.index].id

  dynamic "selection_tag" {
    for_each = var.backup_plan_tag_name
    iterator = tag
    content {
      type  = "STRINGEQUALS"
      key   = tag.value
      value = var.backup_plans[count.index].name
    }
  }

}
