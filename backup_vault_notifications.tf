resource "aws_backup_vault_notifications" "backup_notification" {
  count               = var.use_sns_messaging ? 1 : 0
  backup_vault_name   = aws_backup_vault.backup_vault.name
  sns_topic_arn       = aws_sns_topic.backup_sns_topic[0].arn
  backup_vault_events = var.backup_notify_events
}
