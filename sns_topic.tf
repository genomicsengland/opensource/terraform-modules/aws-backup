resource "aws_sns_topic" "backup_sns_topic" {
  count = var.use_sns_messaging ? 1 : 0
  name  = "${local.resource_prefix}sns_topic"

  kms_master_key_id = var.sns_encryption_kms_key != "" ? var.sns_encryption_kms_key : null
}

resource "aws_sns_topic_policy" "backup_sns_topic_policy" {
  count  = var.use_sns_messaging ? 1 : 0
  arn    = aws_sns_topic.backup_sns_topic[0].arn
  policy = data.aws_iam_policy_document.sns_topic_policy_doc[0].json
}

data "aws_iam_policy_document" "sns_topic_policy_doc" {
  count = var.use_sns_messaging ? 1 : 0
  statement {
    actions = [
      "SNS:Publish",
    ]
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
    resources = [
      aws_sns_topic.backup_sns_topic[0].arn
    ]
    sid = "__default_statement_ID"
  }
}
